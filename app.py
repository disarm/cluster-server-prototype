from flask import Flask, request, jsonify

from invalid_usage import InvalidUsage
from cluster_function import cluster_function
from validation import validate_and_return_configuration, validate_and_return_points

app = Flask(__name__)


@app.route("/", methods=['POST'])
def hello():
    # These functions will raise InvalidUsage if not valid
    validate_and_return_points(request.form['points'])
    validate_and_return_configuration(request.form['configuration'])

    points = request.form['points']
    configuration = request.form['configuration']

    return cluster_function(points, configuration)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

