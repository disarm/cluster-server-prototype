# Python clusterer



To run:
```
docker run --rm -it nicolaidavies/python-clusterer:0.0.8 (cat points.geojson) (cat config.json)
```


The configuration file `config.json` should contain one field `max_elements` which must be a number.


Please note both `points.geojson` and `config.json` must be valid geojson and escaped json. 


Example files: 

Points.geojson:
```
{\r\n  \"type\": \"FeatureCollection\",\r\n  \"features\": [\r\n    {\r\n      \"type\": \"Feature\",\r\n      \"properties\": {},\r\n      \"geometry\": {\r\n        \"type\": \"Point\",\r\n        \"coordinates\": [\r\n          31.1187744140625,\r\n          -26.33772997083918\r\n        ]\r\n      }\r\n    },\r\n    {\r\n      \"type\": \"Feature\",\r\n      \"properties\": {},\r\n      \"geometry\": {\r\n        \"type\": \"Point\",\r\n        \"coordinates\": [\r\n          31.119117736816406,\r\n          -26.337576128760492\r\n        ]\r\n      }\r\n    }\r\n  ]\r\n}
```

Config.json:

```
{ \"max_elements\": 10}
```