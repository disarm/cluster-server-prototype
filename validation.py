from geojson import FeatureCollection
from invalid_usage import InvalidUsage

def validate_and_return_points(points):
    if not 'points' in locals():
        raise InvalidUsage("There are no points", status_code=401)

    # feature_collection = FeatureCollection(points)

def validate_and_return_configuration(configuration):
    if not 'configuration' in locals():
        raise InvalidUsage("There is no configuration", status_code=401)
