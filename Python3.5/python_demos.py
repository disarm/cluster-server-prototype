import json
import geojson
import numpy
import math
from shapely import geometry
from scipy.spatial import ConvexHull
from sklearn import cluster
import sys


# Function to open get geojson features
def points_from_features(geojson_points):
    xy = []
    for gi in geojson_points['features']:
        xy += list(geojson.utils.coords(gi))
    xy = numpy.vstack(xy)
    return(xy)


# Function to dump polygons into a geojson file
def polygons_to_features(polygon_list):
    return(geojson.FeatureCollection(polygon_list))
#def polygons_to_features(polygon_list, output_file):
#    polygon_features = geojson.FeatureCollection(polygon_list)
#    with open(output_file, 'w') as of:
#        json.dump(polygon_list, of)


# Function to define convex hull polygon around a set of points
def convex_hull_polygon(data_points):

    hull = ConvexHull(data_points)
    centroid = numpy.mean(data_points, 0)
    poly_points = []
    for pj in data_points[hull.simplices]:
        poly_points.append(pj[0].tolist())
        poly_points.append(pj[1].tolist())
    poly_points.sort(key=lambda x: numpy.arctan2(x[1] - centroid[1], x[0] - centroid[0]))
    poly_points = poly_points[0::2]  # Remove duplicates
    poly_points.insert(len(poly_points), poly_points[0])
    hull_polygon = geometry.Polygon([v for v in poly_points])

    return(hull_polygon)


## K-means clustering algorithm
#def cluster_by_kmeans(geojson_points, json_config):
#
#    data_points = points_from_features(geojson_points)
#    num_clusters = json_config['number_of_clusters']
#    algo_fit = cluster.KMeans(n_clusters = num_clusters).fit(data_points)
#    clustered_points = [data_points[algo_fit.labels_ == i] for i in range(algo_fit.n_clusters)]
#    cluster_polygons = [convex_hull_polygon(cd) for cd in clustered_points]
#
#    return(polygons_to_features(cluster_polygons))


## Even clustering algorithm
#def cluster_by_size(geojson_points, json_config):
#
#    data_points = points_from_features(geojson_points)
#    size_limit = json_config['max_elements']
#    num_clusters = math.ceil(data_points.shape[0]/size_limit)
#    algo_fit = cluster.KMeans(n_clusters = num_clusters).fit(data_points)
#    centroids = algo_fit.cluster_centers_
#    raw_diff = centroids[:,None,:] - data_points
#    distances = [numpy.sqrt(numpy.sum(rj**2, 1)) for rj in raw_diff]
#
#    if numpy.all(numpy.array([sum(algo_fit.labels_ == i) for i in
#                              range(algo_fit.n_clusters)]) <= size_limit):
#        ix = algo_fit.labels_
#
#    else:
#        ix = numpy.repeat(-1, data_points.shape[0])
#        available = list(range(num_clusters))
#        for ci in range(ix.size):
#            di = numpy.array([d[ci] for d in distances])[available]
#            gi_loc = numpy.argmin(di)
#            gi = available[gi_loc]
#            ix[ci] = gi
#
#            # Check if cluster size limit has been reached
#            if sum(ix == gi) == size_limit:
#                available.pop(gi_loc)
#
#    clustered_points = [data_points[ix == i] for i in range(num_clusters)]
#    cluster_polygons = [convex_hull_polygon(cd) for cd in clustered_points]
#
#    return(polygons_to_features(cluster_polygons))


import random, copy

num_nodes = 40
starting_point = numpy.array([5, 7])
ending_point = numpy.array([ ])

nodes = numpy.array([random.sample(range(100), 2) for x in range(num_nodes)])
ix = numpy.argmin(numpy.sum((nodes - starting_point)**2,1))
route = list(range(num_nodes))
route.pop(ix)
route = [ix] +  random.sample(route, num_nodes - 1)
#random_route = nodes[route]
#tour = random.sample(range(num_nodes),num_nodes)

for temperature in numpy.logspace(0,5,num=100000)[::-1]:
    [i,j] = sorted(random.sample(range(1,num_nodes),2))
    newTour =  route[:i] + route[j:j+1] +  route[i+1:j] + route[i:i+1] + route[j+1:]

    oldDistances =  sum([math.sqrt(sum([(nodes[route[(k+1) % num_nodes]][d] -
                                          nodes[route[k % num_nodes]][d])**2 for d in
                                         [0,1]])) for k in [j,j-1,i,i-1]])
    newDistances =  sum([math.sqrt(sum([(nodes[newTour[(k+1) % num_nodes]][d] -
                                          nodes[newTour[k % num_nodes]][d])**2 for
                                         d in [0,1]])) for k in [j,j-1,i,i-1]])

    #print(( oldDistances - newDistances) / temperature)
    if numpy.abs(math.exp( ( oldDistances - newDistances) / temperature)) > numpy.abs(random.random()):
        route = copy.copy(newTour)

    #if math.exp( ( sum([ math.sqrt(sum([(nodes[route[(k+1) % num_nodes]][d] -
    #                                     nodes[route[k % num_nodes]][d])**2 for d in
    #                                    [0,1] ])) for k in [j,j-1,i,i-1]]) -
    #              sum([math.sqrt(sum([(nodes[newTour[(k+1) % num_nodes]][d] - nodes[newTour[k % num_nodes]][d])**2 for d in [0,1] ])) for k in [j,j-1,i,i-1]])) / temperature) > random.random():
    #   route = copy.copy(newTour);
   #plt.plot(zip(*[nodes[route[i % 15]] for i in range(16) ])[0], zip(*[nodes[route[i % 15]] for i in range(16) ])[1], 'xb-', )

import matplotlib.pyplot as plt
plt.plot(nodes[:,0], nodes[:,1], 'ro')
#plt.plot(random_route[:,0], random_route[:,1], 'r-')
plt.plot(nodes[route,0], nodes[route,1], 'b-')
plt.plot(starting_point[0], starting_point[1], 'ko')
plt.show()



#import matplotlib.pyplot as plt
#with open('../points.geojson') as points_file:
#    X = json.load(points_file)

#Y = {'max_distance':200, 'number_of_clusters':10, 'max_elements':25}

#X = points_from_features(geojson_points = X)
#X = numpy.random.uniform(0, 100, 2*8500).reshape(-1,2)
#clusters_list_1 = cluster_by_kmeans(X, Y)
#clusters_list_2 = cluster_by_size(X, Y)

#plt.plot(X[:,0], X[:,1], 'o')
#for cl in clusters_list:
#    new = numpy.array(list(cl.exterior.coords))
#    plt.plot(new[:,0], new[:,1], 'r-')

#for cl in clusters_list_1:
#    new = numpy.array(list(cl.exterior.coords))
#    plt.plot(new[:,0], new[:,1], 'r-')
#plt.figure()
#
##plt.plot(X[:,0], X[:,1], 'o')
#for cl in clusters_list_2:
#    new = numpy.array(list(cl.exterior.coords))
#    plt.plot(new[:,0], new[:,1], 'g-', lw=2)


