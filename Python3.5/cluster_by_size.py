# Algorithm for making even sized clusters
import json
from utils import *


def cluster_by_size(geojson_points, json_config):

    geojson_points = eval(geojson_points)
    json_config = eval(json_config)
    data_points = points_from_features(geojson_points)
    size_limit = json_config['max_elements']
    num_clusters = math.ceil(data_points.shape[0]/size_limit)
    algo_fit = cluster.KMeans(n_clusters = num_clusters).fit(data_points)
    centroids = algo_fit.cluster_centers_
    raw_diff = centroids[:,None,:] - data_points
    distances = [numpy.sqrt(numpy.sum(rj**2, 1)) for rj in raw_diff]

    if numpy.all(numpy.array([sum(algo_fit.labels_ == i) for i in
                              range(algo_fit.n_clusters)]) <= size_limit):
        ix = algo_fit.labels_

    else:
        ix = numpy.repeat(-1, data_points.shape[0])
        available = list(range(num_clusters))
        for ci in range(ix.size):
            di = numpy.array([d[ci] for d in distances])[available]
            gi_loc = numpy.argmin(di)
            gi = available[gi_loc]
            ix[ci] = gi

            # Check if cluster size limit has been reached
            if sum(ix == gi) == size_limit:
                available.pop(gi_loc)

    clustered_points = [data_points[ix == i] for i in range(num_clusters)]
    cluster_polygons = [convex_hull_polygon(cd) for cd in clustered_points]

    return(polygons_to_features(cluster_polygons))

if __name__ == '__main__':
    geojson_points = json.loads(sys.argv[1])
    json_config = json.loads(sys.argv[2])
    clusters_geojson = cluster_by_size(geojson_points, json_config)
    print(clusters_geojson)
