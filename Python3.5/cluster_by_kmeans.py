# K-means clustering algorithm

from utils import *

geojson_points = sys.argv[1]
json_config = sys.argv[2]

def cluster_by_kmeans(geojson_points, json_config):

    geojson_points = eval(geojson_points)
    json_config = eval(json_config)
    data_points = points_from_features(geojson_points)
    num_clusters = json_config['number_of_clusters']
    algo_fit = cluster.KMeans(n_clusters = num_clusters).fit(data_points)
    clustered_points = [data_points[algo_fit.labels_ == i] for i in range(algo_fit.n_clusters)]
    cluster_polygons = [convex_hull_polygon(cd) for cd in clustered_points]

    return(polygons_to_features(cluster_polygons))

if __name__ == '__main__':
    cluster_by_kmeans()
