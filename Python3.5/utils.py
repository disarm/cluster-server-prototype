import json
import geojson
import numpy
import math
from shapely import geometry
from scipy.spatial import ConvexHull
from sklearn import cluster
import sys


# Function to open get geojson features
def points_from_features(geojson_points):
    xy = []
    for gi in geojson_points['features']:
        xy += list(geojson.utils.coords(gi))
    xy = numpy.vstack(xy)
    return(xy)


# Function to dump polygons into a geojson file
def polygons_to_features(polygon_list):
    return(geojson.FeatureCollection(polygon_list))
#def polygons_to_features(polygon_list, output_file):
#    polygon_features = geojson.FeatureCollection(polygon_list)
#    with open(output_file, 'w') as of:
#        json.dump(polygon_list, of)


# Function to define convex hull polygon around a set of points
def convex_hull_polygon(data_points):

    hull = ConvexHull(data_points)
    centroid = numpy.mean(data_points, 0)
    poly_points = []
    for pj in data_points[hull.simplices]:
        poly_points.append(pj[0].tolist())
        poly_points.append(pj[1].tolist())
    poly_points.sort(key=lambda x: numpy.arctan2(x[1] - centroid[1], x[0] - centroid[0]))
    poly_points = poly_points[0::2]  # Remove duplicates
    poly_points.insert(len(poly_points), poly_points[0])
    hull_polygon = geometry.Polygon([v for v in poly_points])

    return(hull_polygon)
