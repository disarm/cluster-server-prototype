FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apt-get update
RUN apt-get install -y libgeos-dev
RUN pip install --no-cache-dir -r requirements.txt


COPY . .

ENTRYPOINT [ "python", "./Python3.5/cluster_by_size.py"]